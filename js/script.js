$(document).ready(function() {
    let button = $('#button-up');
    $(window).scroll (function () {
    if ($(this).scrollTop () > $(window).height()) {
    button.fadeIn();
} else {
    button.fadeOut();
}
});
    button.on('click', function(){
    $('body, html').animate({
    scrollTop: 0
}, 800);
    return false;
});
    $("#menu").on("click","a", function (event) {
        event.preventDefault();
        let id  = $(this).attr('href'),
            top = $(id).offset().top;
        $('body,html').animate({scrollTop: top}, 1500);
    });

    $("#hide-section").click(function(){
        $("#section-show-hide").slideToggle("slow");
    });
});